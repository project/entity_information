<?php

namespace Drupal\Tests\entity_information\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Tests access to the entity information and settings.
 *
 * @group entity_information
 */
class EntityInformationAccessTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'node',
    'entity_information',
  ];

  /**
   * A regular user with 'access content' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $user;

  /**
   * A regular user with 'access content' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $userWithPermission;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser();
  }

  /**
   * Test access to the entity information page.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testEntityInformationAccess(): void {
    // Create the user with the right permission.
    $this->userWithPermission = $this->drupalCreateUser(['view entity information']);

    $node = $this->drupalCreateNode([
      'type' => 'page',
      'status' => 1,
      'title' => 'Entity Information Node',
    ]);

    // Login as a user without the permission to view the entity information.
    $this->drupalLogin($this->user);

    // Assert this user can not view a node information page.
    $this->drupalGet('node/' . $node->id() . '/information');
    $this->assertSession()->statusCodeEquals(403);

    // Switch to the user with the permission to view the entity information.
    $this->drupalLogout();
    $this->drupalLogin($this->userWithPermission);

    // Assert this user can view a node information page.
    $this->drupalGet('node/' . $node->id() . '/information');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test access to the entity information page.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testSettingsAccess(): void {
    // Create the user with the right permission.
    $this->userWithPermission = $this->drupalCreateUser(['administer entity information settings']);

    // Login as a user without the permission to access the entity information
    // settings page.
    $this->drupalLogin($this->user);

    // Assert this user can not view a node information page.
    $this->drupalGet('/admin/config/system/entity-information');
    $this->assertSession()->statusCodeEquals(403);

    // Switch to the user with the permission to view access the entity
    // information settings page.
    $this->drupalLogout();
    $this->drupalLogin($this->userWithPermission);

    // Assert this user can view a node information page.
    $this->drupalGet('/admin/config/system/entity-information');
    $this->assertSession()->statusCodeEquals(200);
  }

}
