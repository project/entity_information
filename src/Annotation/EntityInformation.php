<?php

namespace Drupal\entity_information\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines an entity information plugin annotation object.
 *
 * @see \Drupal\entity_information\Plugin\EntityInformationManager
 *
 * @Annotation
 */
class EntityInformation extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The entity + bundle combination(s) the plugin supports.
   *
   * Format: [entity].[bundle] for specific entity-bundle combinations or
   * [entity].* for all bundles of the entity.
   *
   * @var string[]
   */
  public array $bundles = [];

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation|string
   *
   * @ingroup plugin_translatable
   */
  public Translation|string $description = '';

  /**
   * The default weight of the plugin.
   *
   * @var int
   */
  public int $weight = 0;

  /**
   * Whether to open the element with the entity information by default.
   *
   * @var bool
   */
  public bool $open = TRUE;

}
