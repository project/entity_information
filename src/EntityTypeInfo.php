<?php

namespace Drupal\entity_information;

/**
 * Manipulates entity type information.
 *
 * This class contains primarily bridged hooks for compile-time or
 * cache-clear-time hooks. Runtime hooks should be placed in EntityOperations.
 */
class EntityTypeInfo {

  /**
   * Adds entity information links to appropriate entity types.
   *
   * This is an alter hook bridge.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   The master entity type list to alter.
   *
   * @see hook_entity_type_alter()
   */
  public function entityTypeAlter(array &$entity_types): void {
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if ($entity_type->hasLinkTemplate('edit-form') && ($entity_type->getFormClass('default') || $entity_type->getFormClass('edit'))) {
        $entity_type->setLinkTemplate('entity-information', '/' . $entity_type_id . '/{' . $entity_type_id . '}/information');
      }
    }
  }

}
