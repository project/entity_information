<?php

namespace Drupal\entity_information;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;

/**
 * Provides the Entity Information plugin manager.
 */
class EntityInformationManager extends DefaultPluginManager {

  /**
   * The url generator service.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected UrlGeneratorInterface $urlGenerator;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * Stores plugged entity types.
   *
   * @var array
   */
  protected array $entityTypes;

  /**
   * Stores bundles per entity type.
   *
   * @var array
   */
  protected array $entityBundles;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new EntityInformationManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match) {
    parent::__construct('Plugin/EntityInformation', $namespaces, $module_handler, 'Drupal\entity_information\Plugin\EntityInformationInterface', 'Drupal\entity_information\Annotation\EntityInformation');

    $this->alterInfo('entity_information_entity_information_info');
    $this->setCacheBackend($cache_backend, 'entity_information_entity_information_plugins');
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * Check if entity information is available for the given entity type.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return bool
   *   Entity information available or not.
   */
  public function isEntityInformationAvailable(string $entity_type_id): bool {
    if (!$plugged_entity_types = $this->getPluggedEntityTypes()) {
      return FALSE;
    }
    return array_key_exists($entity_type_id, $plugged_entity_types);
  }

  /**
   * Get the entity types entity information bundles are available for.
   *
   * @return array|null
   *   The entity types entity information bundles are available for or NULL if
   *   no bundles are found.
   */
  public function getPluggedEntityTypes(): ?array {

    if (empty($this->entityTypes)) {
      // Get all entity information plugin definitions.
      $definitions = $this->getDefinitions();

      // Loop through the definitions to get the bundles for each definition and
      // store it in the entities array.
      foreach ($definitions as $plugin_id => $definition) {
        $entity_bundles = $this->pluggedEntityBundles($definition['bundles']);

        // Loop through the entity/bundle results.
        foreach ($entity_bundles as $entity_bundle) {
          $entity_type = $entity_bundle['entity'];
          $bundle = $entity_bundle['bundle'];
          $this->entityTypes[$entity_type][$bundle]['plugins'][$plugin_id] = [
            'label' => $definition['label'],
            'weight' => $definition['weight'],
            'open' => $definition['open'],
          ];
        }
      }
    }

    return $this->entityTypes;
  }

  /**
   * Returns entity-bundle combinations this plugin supports.
   *
   * If a wildcard bundle is set, all bundles of the entity will be included.
   *
   * @param string[] $entity_bundles
   *   Array of entity-bundle strings that define the bundles for which the
   *   plugin can be used. Format: [entity].[bundle]
   *   '*' can be used as bundle wildcard.
   *
   * @return array
   *   Array of entity and bundle names. Keyed by the [entity].[bundle] key.
   */
  protected function pluggedEntityBundles(array $entity_bundles): array {
    $result = [];

    foreach ($entity_bundles as $entity_bundle) {
      if (strpos($entity_bundle, '.')) {
        [$entity, $bundle] = explode('.', $entity_bundle);
        if ($bundle === '*') {
          foreach ($this->allEntityBundles($entity) as $bundle_from_wildcard) {
            $key = $this->entityBundle($entity, $bundle_from_wildcard);
            $result[$key] = [
              'entity' => $entity,
              'bundle' => $bundle_from_wildcard,
            ];
          }
        }
        else {
          $result[$entity_bundle] = [
            'entity' => $entity,
            'bundle' => $bundle,
          ];
        }
      }
    }

    return $result;
  }

  /**
   * Returns the bundles that are defined for an entity type.
   *
   * @param string $entity
   *   The entity type to get the bundles for.
   *
   * @return string[]
   *   Array of bundle names.
   */
  protected function allEntityBundles(string $entity): array {
    if (!$this->entityTypeManager->hasDefinition($entity)) {
      return [];
    }

    // Only get the bundles for an entity type if we did not already store them.
    if (!isset($this->entityBundles[$entity]) && $definition = $this->entityTypeManager->getDefinition($entity)) {
      $bundle = $definition->getBundleEntityType();
      if ($bundle) {
        $bundles = $this->entityTypeManager
          ->getStorage($bundle)
          ->getQuery()
          ->accessCheck(FALSE)
          ->execute();
      }
      else {
        $bundles = [$entity => $entity];
      }
      $this->entityBundles[$entity] = $bundles;
    }

    // Return the bundles for the entity.
    return $this->entityBundles[$entity];
  }

  /**
   * Creates a key string with entity type and bundle.
   *
   * @param string $entity
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   *
   * @return string
   *   Formatted string.
   */
  protected function entityBundle(string $entity, string $bundle): string {

    return $entity . '.' . $bundle;
  }

  /**
   * Get the entity based on the type and ID from the request.
   *
   * @return \Drupal\Core\Entity\EntityInterface|bool|null
   *   The entity from the request or NULL.
   */
  public function pluggedEntity(): EntityInterface|bool|null {
    $route_parameters = $this->routeMatch->getParameters()->all();

    // We can not handle none or multiple route-parameters.
    if (count($route_parameters) !== 1) {
      return FALSE;
    }

    $keys = array_keys($route_parameters);
    $entity_type_id = reset($keys);
    return $this->entityTypeManager->getStorage($entity_type_id)->load($route_parameters[$entity_type_id]->id());
  }

}
