<?php

namespace Drupal\entity_information\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_information\EntityInformationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class setting form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  public const SETTINGS = 'entity_information.settings';

  /**
   * Entity information manager.
   *
   * @var \Drupal\entity_information\EntityInformationManager
   */
  protected EntityInformationManager $entityInformationManager;

  /**
   * Constructs an entity information SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\entity_information\EntityInformationManager $entity_information_manager
   *   Entity information manager.
   */
  public function __construct(ConfigFactory $config_factory, EntityInformationManager $entity_information_manager) {
    parent::__construct($config_factory);
    $this->entityInformationManager = $entity_information_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.entity_information')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_information_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    if (!$definitions = $this->entityInformationManager->getDefinitions()) {
      return [
        '#markup' => $this->t('No entity information plugins were found.'),
      ];
    }

    $config = $this->config(static::SETTINGS);
    $plugins = [];
    foreach ($definitions as $id => $definition) {
      $plugins[$id] = $definition['label'];
      if (!empty($definition['description'])) {
        $plugins[$id] .= ' - ' . $definition['description'];
      }
    }

    // Sort the plugins by label.
    asort($plugins);

    $form['enabled_plugins'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled entity information plugins'),
      '#default_value' => $config->get('enabled_plugins') ?: [],
      '#options' => $plugins,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('enabled_plugins', $form_state->getValue('enabled_plugins'))
      ->save();
  }

}
