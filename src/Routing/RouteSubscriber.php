<?php

namespace Drupal\entity_information\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\entity_information\EntityInformationManager;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Entity Information routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity information manager service.
   *
   * @var \Drupal\entity_information\EntityInformationManager
   */
  protected EntityInformationManager $entityInformationManager;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   * @param \Drupal\entity_information\EntityInformationManager $entity_information_manager
   *   The entity information manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, EntityInformationManager $entity_information_manager) {
    $this->entityTypeManager = $entity_manager;
    $this->entityInformationManager = $entity_information_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    // Loop through all available entity types and create a route to the
    // information page for each entity type plugins are available for.
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($this->entityInformationManager->isEntityInformationAvailable($entity_type_id)) {
        // Create the route.
        $route = new Route('/' . $entity_type_id . '/{' . $entity_type_id . '}/information');
        $route->addDefaults([
          '_controller' => '\Drupal\entity_information\Controller\EntityInformationController::render',
          '_title' => 'Entity Information',
        ]);
        $route->addRequirements([
          '_permission' => 'view entity information',
        ]);
        $options = [
          'parameters' => [
            $entity_type_id => [
              'type' => 'entity:' . $entity_type_id,
            ],
          ],
          '_admin_route' => TRUE,
        ];
        $route->setOptions($options);

        // Add the route to the collection.
        $collection->add('entity.' . $entity_type_id . '.entity_information', $route);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 100];
    return $events;
  }

}
