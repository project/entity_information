<?php

namespace Drupal\entity_information\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_information\EntityInformationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieves definitions for all entity information plugins.
 */
class EntityInformationLocalTask extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity information manager service.
   *
   * @var \Drupal\entity_information\EntityInformationManager
   */
  protected EntityInformationManager $entityInformationManager;

  /**
   * Creates an EntityInformationLocalTask object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\entity_information\EntityInformationManager $entity_information_manager
   *   The entity information manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityInformationManager $entity_information_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityInformationManager = $entity_information_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_information')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $this->derivatives = [];

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($this->entityInformationManager->isEntityInformationAvailable($entity_type_id) && $entity_type->hasLinkTemplate('entity-information')) {
        $this->derivatives[$entity_type_id . '.entity_information'] = [
          'route_name' => 'entity.' . $entity_type_id . '.entity_information',
          'title' => $this->t('Information'),
          'base_route' => 'entity.' . $entity_type_id . '.canonical',
          'weight' => 99,
        ];
      }
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
