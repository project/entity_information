<?php

namespace Drupal\entity_information\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an interface for Entity Information plugins.
 */
interface EntityInformationInterface extends PluginInspectionInterface {

  /**
   * Builds a renderable array for the field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The field's host entity.
   *
   * @return array
   *   Renderable array.
   */
  public function view(EntityInterface $entity): array;

}
