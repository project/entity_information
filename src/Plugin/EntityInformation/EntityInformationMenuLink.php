<?php

namespace Drupal\entity_information\Plugin\EntityInformation;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;
use Drupal\entity_information\EntityInformationHelperTrait;
use Drupal\entity_information\Plugin\EntityInformationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds menu link content information.
 *
 * @EntityInformation(
 *   id = "menu_link",
 *   label = @Translation("Menu links"),
 *   bundles = {
 *     "node.*",
 *   },
 *   description = @Translation("Overview of available menu link(s) for nodes."),
 *   weight = 15,
 *   open = TRUE
 * )
 */
class EntityInformationMenuLink extends PluginBase implements EntityInformationInterface, ContainerFactoryPluginInterface {

  use EntityInformationHelperTrait;
  use RedirectDestinationTrait;

  /**
   * The menu link manager service.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected MenuLinkManagerInterface $menuLinkManager;

  /**
   * Constructs a LocalActionDefault object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager
   *   The menu link manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MenuLinkManagerInterface $menu_link_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuLinkManager = $menu_link_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.menu.link')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity): array {
    // We can only show menu links when the entity type is available.
    if (!$this->entityTypeManager()->hasDefinition('menu_link_content')) {
      return [
        '#markup' => $this->t('The "Custom menu links" module needs to be enabled to show the menu link information.'),
      ];
    }

    // Get all menu link content items for this entity.
    $menu_link_content_items = $this->menuLinkManager->loadLinksByRoute('entity.node.canonical', ['node' => $entity->id()]);

    // Show a message when no menu link content is found.
    if (empty($menu_link_content_items)) {
      return [
        '#markup' => $this->t('No menu links found for this entity.'),
      ];
    }

    // Build the table header.
    $header = [];
    $header[] = [
      'data' => $this->t('Title'),
      'field' => 'title',
    ];
    $header[] = [
      'data' => $this->t('Menu'),
      'field' => 'menu',
    ];
    $header[] = [
      'data' => $this->t('Language'),
      'field' => 'language_name',
    ];
    $header[] = [
      'data' => $this->t('Operations'),
    ];

    // Build the table rows.
    $destination = $this->getDestinationArray();
    $rows = [];
    /** @var \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent[] $menu_link_content_items */
    foreach ($menu_link_content_items as $menu_link_item) {
      $row = [];

      // Get the menu link content entity.
      $menu_link_content_by_uuid = $this->entityTypeManager()->getStorage('menu_link_content')->loadByProperties([
        'uuid' => $menu_link_item->getDerivativeId(),
      ]);
      /** @var \Drupal\menu_link_content\Entity\MenuLinkContent $menu_link_content */
      $menu_link_content = reset($menu_link_content_by_uuid);

      // Get the menu entity.
      /** @var \Drupal\system\Entity\Menu $menu */
      $menu = $this->entityTypeManager()->getStorage('menu')->load($menu_link_content->getMenuName());

      // Compile the table row and add some operations.
      $row['data']['title'] = $menu_link_content->getTitle();
      $row['data']['menu'] = $menu->label();
      $row['data']['language_name'] = $menu_link_content->language()->getName();

      $operations = [];
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('entity.menu_link_content.edit_form', ['menu_link_content' => $menu_link_content->id()], ['query' => $destination]),
      ];
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('entity.menu_link_content.delete_form', ['menu_link_content' => $menu_link_content->id()], ['query' => $destination]),
      ];
      $row['data']['operations'] = [
        'data' => [
          '#type' => 'operations',
          '#links' => $operations,
        ],
      ];

      // Add the row to the rows array.
      $rows[] = $row;
    }

    // Setup and return the build array for this entity information container.
    return [
      'menu_link_content' => [
        'label' => [
          '#type' => 'item',
          '#title' => $this->t('The following menu links are available for this entity:'),
        ],
        'overview' => [
          '#type' => 'table',
          '#header' => $header,
          '#rows' => $rows,
        ],
      ],
    ];
  }

}
