<?php

namespace Drupal\entity_information\Plugin\EntityInformation;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;
use Drupal\entity_information\EntityInformationHelperTrait;
use Drupal\entity_information\Plugin\EntityInformationInterface;

/**
 * Builds path alias information.
 *
 * @EntityInformation(
 *   id = "path_alias",
 *   label = @Translation("Path aliases"),
 *   bundles = {
 *     "node.*",
 *   },
 *   description = @Translation("Overview of available path alias(es)."),
 *   weight = 10,
 *   open = TRUE
 * )
 */
class EntityInformationPathAlias extends PluginBase implements EntityInformationInterface {

  use EntityInformationHelperTrait;
  use RedirectDestinationTrait;

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity): array {
    // We can only show the aliases when the entity type is available.
    if (!$this->entityTypeManager()->hasDefinition('path_alias')) {
      return [
        '#markup' => $this->t('The "Path" module needs to be enabled to show the path alias information.'),
      ];
    }

    // Get all path aliases for this entity.
    $path_aliases = $this->entityTypeManager()->getStorage('path_alias')->loadByProperties([
      'path' => '/' . $entity->toUrl()->getInternalPath(),
    ]);

    // Show a message when no path alias is found.
    if (empty($path_aliases)) {
      return [
        '#markup' => $this->t('No path alias found for this entity.'),
      ];
    }

    // Build the table header.
    $header = [];
    $header[] = [
      'data' => $this->t('Path Alias'),
      'field' => 'alias',
    ];
    $header[] = [
      'data' => $this->t('Language'),
      'field' => 'language_name',
    ];
    $header[] = [
      'data' => $this->t('Operations'),
    ];

    // Build the table rows.
    $destination = $this->getDestinationArray();
    $rows = [];
    /** @var \Drupal\path_alias\Entity\PathAlias[] $path_aliases */
    foreach ($path_aliases as $path_alias) {
      $row = [];
      $url = $entity->toUrl('canonical', ['language' => $entity->language()]);

      // Compile the table row and add some operations.
      $row['data']['alias'] = Link::fromTextAndUrl($url->toString(), $url);
      $row['data']['language_name'] = $path_alias->language()->getName();

      $operations = [];
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('entity.path_alias.edit_form', ['path_alias' => $path_alias->id()], ['query' => $destination]),
      ];
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('entity.path_alias.delete_form', ['path_alias' => $path_alias->id()], ['query' => $destination]),
      ];
      $row['data']['operations'] = [
        'data' => [
          '#type' => 'operations',
          '#links' => $operations,
        ],
      ];

      // Add the row to the rows array.
      $rows[] = $row;
    }

    // Setup and return the build array for this entity information container.
    return [
      'path_aliases' => [
        'label' => [
          '#type' => 'item',
          '#title' => $this->t('The following path aliases are available for this entity:'),
        ],
        'overview' => [
          '#type' => 'table',
          '#header' => $header,
          '#rows' => $rows,
        ],
      ],
    ];
  }

}
