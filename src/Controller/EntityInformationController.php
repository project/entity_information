<?php

namespace Drupal\entity_information\Controller;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\entity_information\Access\EntityInformationAccessAwareInterface;
use Drupal\entity_information\EntityInformationManager;
use Drupal\entity_information\Form\SettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides route responses for entity information.
 */
class EntityInformationController extends ControllerBase {

  /**
   * The entity information manager service.
   *
   * @var \Drupal\entity_information\EntityInformationManager
   */
  protected EntityInformationManager $entityInformationManager;

  /**
   * EntityInformationController constructor.
   *
   * @param \Drupal\entity_information\EntityInformationManager $entity_information_manager
   *   The entity information manager.
   */
  public function __construct(EntityInformationManager $entity_information_manager) {
    $this->entityInformationManager = $entity_information_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('plugin.manager.entity_information')
    );
  }

  /**
   * Renders the entity information plugins.
   *
   * @return array
   *   The render array.
   */
  public function render(): array {
    // Get the entity we are building the information plugins for.
    $entity = $this->entityInformationManager->pluggedEntity();

    // Get plugged entity types.
    $plugged_entity_types = $this->entityInformationManager->getPluggedEntityTypes();

    // Get plugin configuration te determine which plugins are enabled.
    $enabled_plugins = $this->config(SettingsForm::SETTINGS)->get('enabled_plugins');

    // Get all available entity information plugins for the current entity.
    $plugins = [];
    if (isset($plugged_entity_types[$entity->getEntityTypeId()][$entity->bundle()])) {
      foreach ($plugged_entity_types[$entity->getEntityTypeId()][$entity->bundle()]['plugins'] as $plugin_name => $plugin) {
        if (isset($enabled_plugins[$plugin_name]) && $enabled_plugins[$plugin_name] === $plugin_name && $this->checkPluginAccess($plugin_name, $entity)) {
          $plugins[$plugin_name] = $plugin;
        }
      }
    }

    // Return message if no plugins were found.
    if (count($plugins) === 0) {
      return [
        '#type' => 'markup',
        '#title' => $this->t('Entity information'),
        '#markup' => $this->t("No entity information available. With the right permissions you can enable entity information containers on the @page. It is also possible that access is restricted by the entity information container itself.", [
          '@page' => Link::createFromRoute('configuration page', 'entity_information.settings')->toString(),
        ]),
      ];
    }

    // Sort plugins by weight.
    uasort($plugins, function ($a, $b) {
      return $a['weight'] <=> $b['weight'];
    });

    // Initiate build array.
    $build = [];

    foreach ($plugins as $plugin_id => $plugin) {
      $plugin_from_id = $this->entityInformationManager->createInstance($plugin_id);
      $plugin_definition = $plugin_from_id->getPluginDefinition();
      if (!$plugin_definition) {
        return $build;
      }

      // Create plugin build array.
      $build[$plugin_id]['block'] = [
        '#type' => 'details',
        '#title' => $plugin_definition['label'],
        '#open' => $plugin_definition['open'] ?? TRUE,
        'body' => $plugin_from_id->view($entity),
      ];
    }

    return $build;
  }

  /**
   * Checks if the current user is allowed to access the entity information.
   *
   * @param string $plugin_name
   *   The name of the plugin to check the access for.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check the access for.
   *
   * @return bool
   *   Access is allowed or not.
   */
  protected function checkPluginAccess(string $plugin_name, EntityInterface $entity): bool {
    // Get the plugin and check if it has the access interface implemented. When
    // the interface is not implemented we assume access is allowed.
    $instance = $this->entityInformationManager->createInstance($plugin_name);
    if (!($instance instanceof EntityInformationAccessAwareInterface)) {
      return TRUE;
    }

    // Check the plugin access method. When it returns an AccessResultAllowed
    // result access is allowed.
    $result = $instance->access($this->currentUser(), $entity);
    if ($result instanceof AccessResultAllowed) {
      return TRUE;
    }

    // Access is not allowed.
    return FALSE;
  }

}
