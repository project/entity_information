# Entity Information module

Entity Information provides a new tab for entities holding various sorts
of information detail blocks. Several of these detail blocks are packed with
the module.

Developers can add information to the tab by creating custom entity
information plugins.

## Examples
The module contains several entity information plugins. These also provide a
starting point if you want to create your own plugin.

## Settings
A settings page is available to enable/disable the entity information plugins.

## Plugins
The entity information plugin can be used to provide blocks on the information
tab of entities.

EntityInformation annotation should at least contain:
```
 * @EntityInformation(
 *   id = "plugin_id",
 *   label = @Translation("Detail block name"),
 *   bundles = {
 *     "entity_type.bundle_name"
 *   }
 * )
```

To define a plugin for all bundles of an entity type, use the '*' wildcard:
```
 *   bundles = {
 *     "entity_type.*"
 *   }
```

Other annotation options:
```
 *   description = @Translation("short description of the plugin"),
 *   weight = 10,
 *   open = true
```
